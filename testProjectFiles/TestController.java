package com.hackathon.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.Model.testModel;
import com.hackathon.services.PersistModel;

@RestController
@RequestMapping("/test")
public class TestController {
	@Autowired
	PersistModel service;
	@RequestMapping(value = "/postTest", method = RequestMethod.POST)
	public ResponseEntity<testModel> postTest(@RequestBody testModel model){
		testModel modelOjb = service.saveTestModel(model);
		return new ResponseEntity<>(modelOjb,HttpStatus.OK);
	}
}
