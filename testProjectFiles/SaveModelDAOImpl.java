package com.hackathon.DAO.DAOImpl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.hackathon.DAO.SaveTestModelDAO;
import com.hackathon.Model.testModel;

@Repository
public class SaveModelDAOImpl implements SaveTestModelDAO{
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public testModel saveModel(testModel model) {
		SimpleJdbcInsert insertObj = new SimpleJdbcInsert(jdbcTemplate).withTableName("testtable");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("name", model.getName());
		map.put("emp_id", model.getEmpId());
		insertObj.execute(map);
		return model;
	}

}
