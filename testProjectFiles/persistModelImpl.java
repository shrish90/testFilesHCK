package com.hackathon.services.ServicesImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.DAO.SaveTestModelDAO;
//import com.hackathon.DAO.DAOImpl.SaveModelDAOImpl;
import com.hackathon.Model.testModel;
import com.hackathon.services.PersistModel;
@Service
public class persistModelImpl implements PersistModel{
	
	@Autowired
	SaveTestModelDAO dao;
	@Override
	public testModel saveTestModel(testModel model) {
		testModel obj = dao.saveModel(model);
		return obj;
	}

}
