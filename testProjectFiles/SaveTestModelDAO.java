package com.hackathon.DAO;

import com.hackathon.Model.testModel;

public interface SaveTestModelDAO {
	public testModel saveModel(testModel model);
}
